#pragma once
#include "Room.h"

class RoyalRoom :public Room
{
	static const double ROYAL_ROOM_METER_SQR;
	static const double ROYAL_ROOM_PRICE;

public:
	RoyalRoom(int roomNumber) :Room(roomNumber, ROYAL_ROOM_METER_SQR, ROYAL_ROOM_PRICE) {
	}
};

const double RoyalRoom::ROYAL_ROOM_METER_SQR = 90.0;
const double RoyalRoom::ROYAL_ROOM_PRICE = 400;

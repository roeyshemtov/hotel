#include "Hotel.h"
#include "Employee.h"
#include <iostream>
#include <fstream>

ofstream dbout;
const string EMPLOYEE_DB_NAME = "employees.dat";

Hotel::Hotel()
{

}


Hotel::~Hotel()
{

}

void Hotel::addEmployee(int id, string firstName, string lastName, int employeeId, int jobTypeCode, double salary)
{
	dbout.open(EMPLOYEE_DB_NAME, ios::app);
	Employee employee(id, firstName, lastName, employeeId, jobTypeCode, salary);
	dbout.write((char*)&employee, sizeof(Employee));
	dbout.close();
}

void Hotel::displayEmployees()
{
	ifstream fin(EMPLOYEE_DB_NAME, ios::in);
	Employee employee;
	while (!fin.eof())
	{
		fin.read((char*)&employee, sizeof(Employee));
		employee.print();
	}
	fin.close();
}

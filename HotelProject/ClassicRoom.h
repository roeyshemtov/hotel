#pragma once
#include <iostream>
#include "Room.h"

class ClassicRoom :public Room
{
	static const double CLASSIC_ROOM_METER_SQR;
	static const double CLASSIC_ROOM_PRICE;
public:
	ClassicRoom(int roomNumber):Room(roomNumber,ClassicRoom::CLASSIC_ROOM_METER_SQR, ClassicRoom::CLASSIC_ROOM_PRICE) {
	}
};

const double ClassicRoom::CLASSIC_ROOM_METER_SQR = 45.0;
const double ClassicRoom::CLASSIC_ROOM_PRICE = 100;

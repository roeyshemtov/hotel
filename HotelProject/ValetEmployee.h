#pragma once
#include "Employee.h"

class ValetEmployee : public Employee
{
	static const int VALET_JOB_TYPE_CODE;
	static const double VALET_JOB_SALARY;

public:
	ValetEmployee(int id, string firstName, string lastName, int employeeId)
		:Employee(id, firstName, lastName,employeeId,VALET_JOB_TYPE_CODE,VALET_JOB_SALARY) {
	};

};
const int ValetEmployee::VALET_JOB_TYPE_CODE = 0;
const double ValetEmployee::VALET_JOB_SALARY = 4000;

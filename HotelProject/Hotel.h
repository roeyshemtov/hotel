#pragma once
#include <string>
using namespace std;

class Hotel
{
public:
	Hotel();
	~Hotel();
	void addEmployee(int id, string firstName, string lastName, int employeeId, int jobTypeCode, double salary);
	void displayEmployees();
private:
};

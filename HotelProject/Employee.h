#pragma once
#pragma once
#include "Person.h"
class Employee : public Person
{
public:
	Employee() {
		Person();
	};
	Employee(int id, string firstName, string lastName, int employeeId, int jobTypeCode, double salary)
		:Person(id,firstName,lastName) {
		this->employeeId = employeeId;
		this->jobTypeCode = jobTypeCode;
		this->salary = salary;
	};

private:
	int employeeId;
	double salary;
	int jobTypeCode;
};

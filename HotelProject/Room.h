#pragma once
#include <iostream>

using namespace std;
class Room
{
public:
	void print() {
		cout << "Room Number: " << this->roomNumber << ", Meter squar: " << this->meterSqr 
			<<", Price: " << this->priceForNight;
	}
	void checkInRoom() {
		this->isAviable = false;
	}
	void checkoutRoom() {
		this->isAviable = true;
	}
	bool isRoomAviable(){
		return this->isAviable;
	}
protected:
	Room(int roomNo, double mSqr,double price) {
		this->roomNumber = roomNo;
		this->meterSqr = mSqr;
		this->priceForNight = price;
		this->isAviable = true;
	};
private:
	int roomNumber;
	double meterSqr;
	double priceForNight;
	bool isAviable;
};
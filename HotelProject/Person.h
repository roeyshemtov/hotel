#pragma once
#include <string> 
#include <iostream> 
using namespace std;

class Person
{
public:
	Person() {};
	Person (int id, string firstName, string lastName) {
		this->id = id;
		this->firstName = firstName;
		this->lastName = lastName;
	};
	virtual void print() {
		cout << "Person id: " << this->id << ", firstName: " 
			<< this->firstName << ", lastName: " << this->lastName;
	};
private:
	int id;
	string firstName;
	string lastName;
};


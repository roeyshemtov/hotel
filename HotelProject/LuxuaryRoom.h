#pragma once
#include "Room.h"

class LuxuaryRoom :public Room
{
	static const double LUXUARY_ROOM_METER_SQR;
	static const double LUXUARY_ROOM_PRICE;

public:
	LuxuaryRoom(int roomNumber) :Room(roomNumber, LUXUARY_ROOM_METER_SQR, LUXUARY_ROOM_PRICE) {
	}
};

const double LuxuaryRoom::LUXUARY_ROOM_METER_SQR = 60.0;
const double LuxuaryRoom::LUXUARY_ROOM_PRICE = 200;
